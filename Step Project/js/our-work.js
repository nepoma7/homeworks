const workGallery = document.querySelector('.js-workGallery');
const workMenu = document.querySelector('.js-workMenu');
const workLoadMore = document.querySelector('.js-workLoadMore');
const workGalleryData = {
    '#all': { from: 0, qty: 12 },
    '#graphicDesign': { from: 3, qty: 4 },
    '#webDesign': { from: 5, qty: 4 },
    '#landingPages': { from: 0, qty: 8 },
    '#wordpress': { from: 0, qty: 6 }
};

loadGallery(workGalleryData['#all']);

workLoadMore.addEventListener('click', function (){
    loadGallery({ from: 0, qty: 12 }, false);
    this.style.opacity = 0;
    setTimeout(()=>this.remove(), 350);
});
workMenu.addEventListener('click', function (e){
    if (e.target.tagName === 'A') {
        e.preventDefault();
        const currActive = this.querySelector('.active');
        if (currActive) currActive.classList.remove('active');
        e.target.parentNode.classList.add('active');

        loadGallery(workGalleryData[e.target.getAttribute('href')]);
    }
});

function loadGallery(articles, isClear = true) {
   if(isClear) workGallery.innerHTML = '';
        
    for (let i = articles.from; i < articles.qty + articles.from; i++) {
        workGallery.innerHTML += `
            <div class="our-work__card">
                <img src="img/works/work${i + 1}.jpg" alt="our work${i + 1}" class="our-work__image">
                <div class="our-work__hover">
                    <div class="our-work__hover-image">
                        <img src="img/hover-icon.svg" alt="hover icon" class="our-work__hover-image__icon">
                    </div>
                    <h3 class="our-work__hover-title">creative design ${i || ''}</h3>
                    <p class="our-work__hover-text">web design ${i || ''}</p>
                </div>
            </div>
        `;            
    }
}