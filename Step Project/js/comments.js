const commentsGallery = document.querySelector('.js-commentsGallery');
const commentsPanel = document.querySelector('.js-commentsPanel');

const slider = {
    curr: 3,
    qty: 4,
    update: function() {
        if (this.curr < 1) this.curr = this.qty;
        if (this.curr > this.qty) this.curr = 1;

        const item = commentsPanel.querySelector('.active');
        if (item) item.classList.remove('active');
        
        const currItem = commentsPanel.querySelector(`li:nth-child(${this.curr})`);
        if (currItem) currItem.classList.add('active');


        const comment = commentsGallery.querySelector('.active');
        if (comment) comment.classList.remove('active');
        
        const currComment = commentsGallery.querySelector(`article:nth-child(${this.curr})`);
        if (currComment) currComment.classList.add('active');
    }
}

commentsPanel.addEventListener('click', function(e) {
    if (e.target.dataset.button === 'left' || e.target.parentNode.dataset.button === 'left') {
        slider.curr--;
        slider.update();
    } else if (e.target.dataset.button === 'right' || e.target.parentNode.dataset.button === 'right') {
        slider.curr++;
        slider.update();
    } else if (e.target.dataset.item || e.target.parentNode.dataset.item) {
        const numberImage = e.target.dataset.item || e.target.parentNode.dataset.item;
        slider.curr = +numberImage;
        slider.update();
    }
});