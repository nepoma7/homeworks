const newsGallery = document.querySelector('.js-newsGallery');

newsGallery.innerHTML = '';
for (let i = 0; i < 8; i++) {
    const i2 = i % 2;
    newsGallery.innerHTML += `
        <div class="news__card">
            <img src="img/news/news${i2 + 1}.png" alt="news${i2+ 1}" class="news__image">
            <div class="news__panel">
                <h3 class="news__title-card">Amazing Blog Post</h3>
                <p class="news__text-card">By admin    |    2 comment</p>
            </div>
            <div class="news__mark">
                12<br>Feb
            </div>
        </div>
    `;            
}