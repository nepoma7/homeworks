console.log(calc(+prompt('Input a'), +prompt('Input b'), prompt('Input operation: +, -, *, /')));

function calc(a, b, operation) {
    if(operation === '+') return a + b;
    if(operation === '-') return a - b;
    if(operation === '*') return a * b;
    return a / b;
}